/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.environment

import java.util.UUID
import org.arakhne.afc.gis.mapelement.MapCircle
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * Area of fog. The visibility field describes the maximum distance of perception for the agents
 * when they are into the fog area.
 *
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
class FogZone extends MapCircle {

	@Accessors(PUBLIC_GETTER)
	val visibility : double

	new (id : UUID, x : double, y : double, radius : double, visibility : double) {
		super(id, x, y, radius)
		this.visibility = Math::max(0.0, visibility)
	}

}
