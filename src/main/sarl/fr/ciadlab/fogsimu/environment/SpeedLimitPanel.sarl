/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.environment

import java.util.UUID
import org.arakhne.afc.gis.road.primitive.RoadType
import org.arakhne.afc.math.geometry.d1.d.Point1d
import org.arakhne.afc.math.geometry.d2.d.Point2d
import org.arakhne.afc.simulation.framework.framework1d.environment.AbstractMapPointSideRoadObject

import static extension org.arakhne.afc.simulation.framework.framework1d.environment.RoadSpeedLimit.*
import org.arakhne.afc.simulation.framework.framework1d.environment.ObjectType

/**
 * A panel at the side of the road that is able to provide a speed limit information 
 *
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
class SpeedLimitPanel extends AbstractMapPointSideRoadObject {

	var speedLimit : int

	new (id : UUID, position1d : Point1d, position2d : Point2d, content : String = null) {
		super(id, position1d, 1, 1, position2d.x, position2d.y)
		this.speedLimit = content.parseSpeedLimit.adjustSpeedLimit
	}

	def getObjectType : ObjectType {
		return ObjectType::ROAD_SIGN
	}

	@SuppressWarnings("potential_inefficient_value_conversion")
	protected static def parseSpeedLimit(content : String) : int {
		return content as int
	}

	/**
	 * @return the speed limit in km/h
	 */
	def getSpeedLimit : int {
		if (this.speedLimit === 0) {
			var segment = this.roadSegment
			var typeAttr = segment.getAttribute("TYP_DONNEE")
			var type = typeAttr?.string?.toLowerCase
			switch (type) {
				case "urbain": {
					this.speedLimit = RoadType::LOCAL_ROAD.getDefaultSpeedLimitFor
				}
			}
			if (this.speedLimit === 0) {
				this.speedLimit = segment.roadType.getDefaultSpeedLimitFor
			}
		}
		return this.speedLimit
	}

	def setSpeedLimit(content : String) {
		this.speedLimit = content.parseSpeedLimit.adjustSpeedLimit
	}

	def setSpeedLimit(limit : int) {
		this.speedLimit = limit.adjustSpeedLimit
	}

}
