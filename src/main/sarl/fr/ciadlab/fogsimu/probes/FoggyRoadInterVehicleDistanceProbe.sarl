/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.probes

import org.arakhne.afc.gis.road.primitive.RoadSegment
import org.arakhne.afc.simulation.framework.framework1d.probes.^def.GlobalRoadInterVehicleDistanceProbe

import static extension fr.ciadlab.fogsimu.utils.FogUtils.*

/**
 * A probe for measuring the average
 * inter-vehicle distance on a foggy road.
 *
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
class FoggyInterVehicleDistanceProbe extends GlobalRoadInterVehicleDistanceProbe {

	protected override getRoadFilter : (RoadSegment)=>boolean {
		[
			it.isFoggy
		]
	}

}
