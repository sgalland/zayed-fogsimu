/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.fx.drawers

import fr.ciadlab.fogsimu.preferences.FogPreferenceManager
import javafx.scene.paint.Color
import org.arakhne.afc.gis.road.RoadPolyline
import org.arakhne.afc.nodefx.ZoomableGraphicsContext
import org.arakhne.afc.simulation.framework.framework1d.fx.drawers.TrafficRoadPolylineDrawer

import static extension fr.ciadlab.fogsimu.utils.FogUtils.*
import static extension org.arakhne.afc.simulation.framework.base.preferences.PreferenceManagerSingleton.*

/** Drawer of the roads and the traffic with fog color on roads.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
class FogTrafficRoadPolylineDrawer extends TrafficRoadPolylineDrawer {

	def getFogColor(gc : ZoomableGraphicsContext) {
		gc.rgb(typeof(FogPreferenceManager).singleton.fogColorOnRoads)
	}

	protected override setupRoadInterior(gc : ZoomableGraphicsContext, element : RoadPolyline) {
		var color : Color
		if (element.isSelected) {
			color = Color::RED
		} else if (element.isFoggy) {
			color = gc.getFogColor
		} else {
			color = gc.rgb(ROAD_COLOR)
		}
		gc.setStroke(color);
		if (element.isWidePolyline()) {
			gc.setLineWidthInMeters(element.getWidth());
		} else {
			gc.setLineWidthInPixels(1);
		}
	}

}
