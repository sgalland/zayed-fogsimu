/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu

import java.io.File
import java.util.Random
import org.arakhne.afc.gis.io.shape.GISShapeFileWriter
import org.arakhne.afc.gis.road.RoadPolyline
import org.arakhne.afc.gis.road.primitive.RoadType
import org.arakhne.afc.gis.road.primitive.TrafficDirection
import org.arakhne.afc.io.dbase.DBaseFileWriter
import org.arakhne.afc.io.shape.ESRIBounds
import org.arakhne.afc.io.shape.ShapeElementType
import org.arakhne.afc.io.shape.ShapeFileIndexWriter
import org.arakhne.afc.math.geometry.d2.d.Rectangle2d
import org.arakhne.afc.vmutil.FileSystem

/** Generator of map.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
@SuppressWarnings("all")
class MapGenerator {

	static val RANDOM = new Random

	static def createRoad(i : int, lines : double[]) : RoadPolyline {
		var rline = new RoadPolyline
		val onSegmentDirection = true //RANDOM.nextBoolean
		if (onSegmentDirection) {
			rline.addPoint(lines.get(i), lines.get(i + 1))
			rline.addPoint(lines.get(i + 2), lines.get(i + 3))
			rline.trafficDirection = TrafficDirection::ONE_WAY
		} else {
			rline.addPoint(lines.get(i + 2), lines.get(i + 3))
			rline.addPoint(lines.get(i), lines.get(i + 1))
			rline.trafficDirection = TrafficDirection::NO_ENTRY
		}
		rline.roadType = RoadType::FREEWAY
		return rline
	}
	
	@SuppressWarnings("discouraged_reference")
	static def main(args : String*) {
		var line1 : double[] = #[
			938620, 2302823,
			939620, 2302950,
			940120, 2303050,
			942420, 2303298,
			942920, 2303397,
			944010, 2303490,
			945368, 2303439,
			945734, 2303386,
			946898, 2303446,
			947024, 2303494
		]

		var line2 : double[] = #[
			938620, 2302830.5,
			939620, 2302957,
			940120, 2303056.5,
			942420, 2303305,
			942920, 2303403.5,
			944010, 2303496.5,
			945368, 2303446,
			945734, 2303392.5,
			946898, 2303453,
			947023.5, 2303500.5
		]

		var lines = <RoadPolyline>newArrayList

		for (var i = 0; (i+3) < line1.length; i = i + 2) {
			var rline1 = createRoad(i, line1)
			lines += rline1

			var rline2 = createRoad(i, line2)
			lines += rline2
		}

		var bounds : Rectangle2d = null
		for (line : lines) {
			if (bounds === null) {
				bounds = line.boundingBox
			} else {
				bounds= bounds.createUnion(line.boundingBox)
			}
		}
		System.out.println(bounds)
		
		val currentFolder = new File(FileSystem::CURRENT_DIRECTORY).absoluteFile
		System.out.println("Current folder: " + currentFolder.absolutePath)
		
		var shpFile = new File(currentFolder, "data/shp/map-line2.shp")
		var dbfFile = new File(currentFolder, "data/shp/map-line2.dbf")
		var shxFile = new File(currentFolder, "data/shp/map-line2.shx")

		var dbfWriter = new DBaseFileWriter(dbfFile)
		
		var esriBounds = new ESRIBounds(bounds.minX, bounds.maxX, bounds.minY, bounds.maxY)
		var shxWriter = new ShapeFileIndexWriter(shxFile, ShapeElementType::POLYLINE, esriBounds)

		var shpWriter = new GISShapeFileWriter(
			shpFile, ShapeElementType::POLYLINE, bounds,
			dbfWriter, shxWriter)
		shpWriter.write(lines)
		shpWriter.close
		System.out.println("done")
	}

}
