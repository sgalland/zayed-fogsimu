/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.skills

import com.google.common.util.concurrent.AtomicDouble
import fr.ciadlab.fogsimu.events.I2VFogConditionDetected
import fr.ciadlab.fogsimu.preferences.FogPreferenceManager
import java.util.List
import java.util.concurrent.atomic.AtomicInteger
import org.arakhne.afc.gis.road.path.RoadPath
import org.arakhne.afc.math.geometry.d1.Direction1D
import org.arakhne.afc.math.geometry.d1.d.Point1d
import org.arakhne.afc.math.geometry.d2.d.Point2d
import org.arakhne.afc.simulation.framework.framework1d.environment.PerceptionUnit1d
import org.arakhne.afc.simulation.framework.framework1d.environment.RoadObject

import static extension org.arakhne.afc.math.physics.MeasureUnitUtil.*
import static extension org.arakhne.afc.simulation.framework.base.preferences.PreferenceManagerSingleton.*

/** 
 * Skill for managing the cruise speed.
 * The cruise speed is one of the following:<ol>
 * <li>the speed that is indicated by <i>on-board alert system</i>; or</li>
 * <li>the speed that is indicated by the last encountered <i>speed limit panel</i>; or</li>
 * <li>the legal speed that is associated to the <i>type of road</i> if there is no encountered speed limit panel.</li>
 * </ol>
 *
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
skill I2VPanelRoadCruiseSpeedManager extends PanelRoadCruiseSpeedManager {

	val fogConditionDistance = new AtomicDouble(Double::NaN)

	val fogConditionSpeed = new AtomicInteger(-1)

	var theta : double

	protected var lastPosition : Point2d
	
	override install {
		this.theta = typeof(FogPreferenceManager).singleton.fogDetectionDispatchDistanceI2V
	}

	override updateCruiseSpeed(
		t : double, perceivedObjects : List<PerceptionUnit1d<RoadObject>>,
		path : RoadPath, currentPosition : Point1d, currentDirection : Direction1D,
		currentSpeed : double, maxSpeed : double) {

		val position = new Point2d
		currentPosition.segment.projectsOnPlane(currentPosition.curvilineCoordinate, currentPosition.lateralDistance,
			position, null)
		this.lastPosition = position

		super.updateCruiseSpeed(t, perceivedObjects, path, currentPosition, currentDirection, currentSpeed, maxSpeed)

		// CAUTION: Forget the fog condition signal on-board
		val fp = this.fogConditionSpeed.getAndSet(-1)
		if (fp > 0) {
			var preconizedSpeedLimit : double = fp.kmh2ms
			val foggyWeatherSpeed = preconizedSpeedLimit.applyRoadShape(path, currentPosition, currentDirection,
				currentSpeed, maxSpeed)
			this.cruiseSpeed = Math::min(this.cruiseSpeed, foggyWeatherSpeed)
		}
	}

	def fogDectionSignalReceived(^event : I2VFogConditionDetected) {
		val position = this.lastPosition
		if (position !== null) {
			val signalDistance = position.getDistance(^event.signalSourcePosition)
			if (signalDistance <= this.theta) {
				var newSignal = false
				val cd = this.fogConditionDistance.get
				val d = position.getDistance(^event.fogPosition)
				if (cd.isNaN) {
					// First time a fog condition event is received
					newSignal = true
				} else {
					// Already received a fog condition event
					if (d < cd) {
						newSignal = true
					}
				}
				if (newSignal) {
					this.fogConditionDistance.set(d)
					this.fogConditionSpeed.set(^event.speedLimitOnPanel)
				}
			}
		}
	}

}
