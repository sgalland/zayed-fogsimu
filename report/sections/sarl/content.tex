\chapter{SARL~Language and Janus~Framework}\label{section:janussarl}

\begin{graphicspathcontext}{{./img/sarl/},\old}

\section{SARL Agent-Programming Language}\label{section:sarl}

SARL\footnote{Official SARL website: \url{http://www.sarl.io}}\index{SARL} is a general-purpose agent-oriented programming language \citep{RodriguezGaudGalland2014_725}.
This language aims at providing the fundamental abstractions for dealing with concurrency, distribution, interaction, decentralization, reactivity, autonomy and dynamic reconfiguration.
The first definition of SARL was proposed in \citeyear{RodriguezGaudGalland2014_725} by \citet{RodriguezGaudGalland2014_725}. Its initial metamodel\index{SARL!Metamodel} was defined between \citeyear{CossentinoGaudHilaireGallandKoukam2010_1} and \citeyear{RodriguezGaudGalland2014_725} based on the Capacity-Role-Interaction-Organization metamodel\index{SARL!CRIO Metamodel} \citep{CossentinoGaudHilaireGallandKoukam2010_1}.
The main perspective that guided the creation of SARL is the establishment of an open and easily-extensible language.
Such language should thus provide a reduced set of key concepts that focuses solely on the principles considered as essential to implement a MAS.
This observation was make by the authors of SARL, who are also the authors of the Janus agent framework\index{Janus}\index{SARL!SARL Run-time Environment} \citep{GaudGallandHilaireKoukam2009_16, GallandGaudRodriguezHilaire2010_20, GallandRodriguezGaud2017_982} since \citeyear{GaudGallandHilaireKoukam2009_16}. Based on several experiences and interviews of Janus users, the need to make the agent-oriented concepts easier to implement with an easy-to-use programming language (e.g. Python) arises.

The major concepts of SARL are explained below, and illustrated in Figure \figref{sarlconcepts}.
The detailed definitions of these concepts could be found in \citep{CossentinoGaudHilaireGallandKoukam2010_1}.


\mfigure[t]{width=\linewidth}{SARL-Model_SARL_Simple}{Major concepts in the SARL metamodel}{sarlconcepts}

\subsection{Agent and Behavior}

\emph{An agent\index{SARL!Metamodel!Agent} is an autonomous entity having a set of skills\index{SARL!Metamodel!Skill} to realize the capacities\index{SARL!Metamodel!Capacity} it exhibits}.
An agent has a set of built-in capacities considered essential to respect the commonly accepted competences of agents, such as autonomy, reactivity, pro-activity and social capacities.
The full set of Built-In Capacities\index{SARL!Metamodel!Capacity!Built-In Capacity} (BIC) are presented in Section~\ref{section:BIC}.
Among these BIC, \code{Behaviors} capacity\index{SARL!Metamodel!Capacity!Behaviors} enables agents to incorporate a collection of behaviors that determine their global conducts.
An agent has also a default behavior directly described within its definition.
\emph{Behavior maps a collection of perceptions represented by Events\index{SARL!Metamodel!Event} to a sequence of Actions\index{SARL!Metamodel!Action}.}
Each action is a subroutine that may use fields (variables) that are declared within the behavior in order to realize its tasks.
The various behaviors of an agent communicate using an event-driven approach by default.
\emph{An event\index{SARL!Metamodel!Event} is the specification of some occurrence in a space that may potentially trigger effects by a listener}, e.g. agent\index{SARL!Metamodel!Agent} or behavior\index{SARL!Metamodel!Behavior}. 
SARL is independent of, but supports any agent architecture\index{SARL!Agent Architecture}, such as Belief-Desire-Intention\index{BDI} \citep{Rao.95.bdi} or Subsumption\index{Subsumption} \citep{brooks90}. In other words, it is possible to implement any agent architecture with SARL; And, SARL is not restricted to a specific agent architecture.

For clarifying the constructs of SARL, it is necessary to describe the definition of an agent \code{FactorialAgent} that can compute a factorial by querying another agent to compute the value of $fact(n-1)$.
This agent waits for another agent's request to calculate (\code{Calculate} event) a factorial.
Once computed, it notifies the result using the \code{ComputationDone} event.

\begin{lstlisting}[caption={Computation of Factorial with a SARL Agent.}, label={agentcode}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
event Factorial {
  var upto : int
  var number : int
  var value : int
} 
event Calculate { (*@\label{fa:def_calculate}@*) 
  var number : int 
} 
event ComputationDone { 
  var result : int 
}
agent FactorialAgent { (*@\label{fa:def_agent}@*)
  uses Lifecycle, Behaviors, DefaultContextInteractions
  on Factorial [occurrence.number < occurrence.upto ] { (*@\label{fa:evt_fact_1}@*) 
    wake(new Factorial => [ 
         upto = occurrence.upto
         number = occurrence.number + 1
         value = occurrence.value * (occurrence.number + 1)
      ])
  }
  on Factorial [ occurrence.number == occurrence.upto ] { (*@\label{fa:evt_fact_2}@*) 	
    emit(new ComputationDone => [ result = occurrence.value ])	
    killMe
  }
  on Calculate {(*@\label{fa:evt_cal}@*) 	
    wake(new Factorial => [ (*@\label{fa:wake_in_calculate}@*) 	
         upto = occurrence.number (*@\label{fa:assing_upto}@*)
         number = 0
         value = 1
      ])
  }
}
\end{lstlisting}

An agent is declared by the \code{agent} keyword (Line~\ref{fa:def_agent}). 
In the agent's body block, mental state (in the form of attributes), actions (or functions) and event handlers are declared.
The actions that an agent can perform are specified by capacities or natively inside the agent definition, inside actions.
The keyword \code{uses} imports the actions defined in capacities so that they can be accessed directly as an agent native action.

The agent must then declare its perceptions and the sequence of actions it wants to perform for each perception.
This is achieved using the clause \code{on <perception> [<guard>] \{<body>\}}.
The \code{FactorialAgent} declares three behavioral event handlers (Lines~\ref{fa:evt_fact_1}, \ref{fa:evt_fact_2}, and \ref{fa:evt_cal}). 

Perceptions for SARL agents take the form of events, and they can be declared using the \code{event} keyword.
For instance, the \code{Calculate} event is defined in line~\ref{fa:def_calculate}.
Events can carry information within fields, and in this case, the number is factorial.

When the \code{Calculate} event is perceived (Line~\ref{fa:evt_cal}), the agent can access the event's instance using the \code{occurrence} keyword.
At line~\ref{fa:assing_upto}, it sets the \code{upto} attribute using the information for the \code{Calculate} event occurrence.

From this point, the agent should start computing the factorial.
The \code{Behaviors} built-in capacity provides the agent with mechanisms to register/unregister new behaviors and fire new internal events (\code{wake} action).
To calculate the factorial, it fires an internal event of type \code{Factorial}, using the \code{wake} action.

Two behaviors are declared for \code{Factorial} (Line~\ref{fa:evt_fact_1} and \ref{fa:evt_fact_2}). 
When an event is perceived, SARL agents execute all their behaviors for that event type concurrently.
Behaviors can declare guards to prevent their execution if required.
The behavior at line~\ref{fa:evt_fact_1} is only executed if \code{occurrence.number < occurrence.upto} is evaluated to be true.
This behavior simply calculates the factorial for the next integer and fires a \code{Factorial} event again.

Likewise, when the factorial for the requested number (stored in the \code{upto} attribute) is found, the behavior at line~\ref{fa:evt_fact_2} is executed.
The \code{emit} action fires an event in the \emph{default interaction space} within the \emph{default context} to notify that the computation is finished. 
After that, agent stops its execution using the \code{killMe} action from the \code{Lifecycle} capacity.

It is necessary to understand the difference between the \code{wake} and \code{emit} actions clearly. 
\code{wake} action fires an internal event within the agent; and this event may be perceived by the behaviors of the agent and its members.
While \code{emit} action enables to fire an event in a given space.


\subsection{Capacity and Skill}\label{section:capacities}

\emph{An action\index{SARL!Metamodel!Action} is a specification of a transformation of a part of the designed system or its environment.}
This transformation guarantees resulting properties if the system before the transformation satisfies a set of constraints.
An action is defined in terms of pre- and post-conditions.
\emph{A capacity\index{SARL!Metamodel!Capacity} is the specification of a collection of actions\index{SARL!Metamodel!Action}}.
This specification makes no assumptions about its implementation.
It could be used to specify what an agent can do, what a behavior requires for its execution.
\emph{A skill\index{SARL!Metamodel!Skill} is a possible implementation of a capacity\index{SARL!Metamodel!Capacity} fulfilling all the constraints of this specification}. 
An agent\index{SARL!Metamodel!Agent} can dynamically evolve by learning/acquiring new capacities, but it can also dynamically change the skill associated to a given capacity \citep{RodriguezGaudHilaireGallandKoukam2006_115, CossentinoGallandGaudHilaireKoukam2008_13}. 
Acquiring new capacities enables an agent to get access to new behaviors\index{SARL!Metamodel!Behavior}.
This provides agents with a self-adaptation mechanism that allow them to dynamically change their architecture according to their current needs and goals.


\subsection{Context and Spaces}

\emph{A context\index{SARL!Metamodel!Context} defines the perimeter/boundary of a sub-system, and gathers a collection of spaces\index{SARL!Metamodel!Space}}.
In each context, there is at least one particular space called \emph{default space}\index{SARL!Metamodel!Space!Default Space} to which all agents in this context belong.
This ensures the existence of a commonly shared Space\index{SARL!Metamodel!Agent} to all agents in the same context.
Each agent can then create specific public or private spaces\index{SARL!Metamodel!Space} to achieve its personal goals. 
Since their creation, agents are incorporated into a context called the \emph{Default Context}\index{SARL!Metamodel!Context!Default Context} (see the upper part of Figure \figref{holon}, level n).
The notion of Context makes complete sense when agents are considered composed or holonic (see Section~\ref{section:holon}).


\subsection{Recursive Agent and Hierarchical Multiagent System}\label{section:holon}

\mfigure[t]{width=.9\linewidth}{HolonContext}{A holon or a recursive agent in SARL}{holon}


In 1967, Arthur Koestler coined the term \emph{holon}\index{Holon} as an attempt to conciliate holistic and reductionist visions of the world. 
A holon represents a part-whole construct that can be seen as a component of a higher-level system or as whole composed of other self-similar holons as substructures \citep{Koestler1967Ghost}.
Holonic systems\index{Holon!Holonic Systems} grow from the need to find comprehensive construct that could help explain social phenomena. 
Since then, it is being used in a wide range of domains, including philosophy \citep{Wilber2000Sex}, manufacturing systems \citep{Leeuwen1997Holons}, and MAS \citep{Gerber1999Holonic}.

Several works have studied this question, and they have proposed a number of models inspired from their experience in different domains. 
It can be seen from many cases that \emph{agents are composed of other agents}. 
Each researcher gives a specific name to this type of agent; \citet{Ferber1999MultiAgent} discusses \emph{individual} and \emph{collective} agents; \emph{meta-agents} are proposed by \citet{Holland1995Hidden}; \emph{Agentified Groups} are taken into account in the work of \citet{Odell2005Metamodel}.
All of these are only examples of how researchers have called these ``aggregated'' entities that are composed of lower level agents.
More recently, the importance of holonic MAS has been recognized by different methodologies such as ASPECS\index{ASPECS} \citep{CossentinoGaudHilaireGallandKoukam2010_1} and O-MASE \citep{Case2013Applying}.

In SARL\index{SARL}, agents can be composed of other agents. 
Therefore, each of these SARL agents is called holon\index{Holon}.
It is interesting to note that a holon may be part of more than other holons.
Consequently, the structure composed by the agents is not a strict hierarchy but a multi-hierarchy, called holarchy\index{Holon!Holarchy}.
In order to achieve this, SARL agents are self-similar structures that compose each other via their contexts.
Each agent defines its own context, called \emph{inner context}\index{SARL!Metamodel!Context!Inner Context}, and it is part of one or more \emph{external contexts}\index{SARL!Metamodel!Context!External Context}. 
For instance, in Figure~\ref{fig:holon}, agent A is taking part of two \emph{external contexts} (i.e. Default Context and External Context 1) and has its own \emph{inner context} where the agents B, C, D and E evolve.

\subsection{Built-in Capacities}\label{section:BIC}

Every agent in SARL has a set of Built-in Capacities\index{SARL!Metamodel!Context!Inner Context} (BIC) considered essential to respect the commonly accepted competences of agents.
These capacities are considered the main building blocks on top of which other higher-level capacities and skills can be constructed.
They are defined in the SARL language specifications, but the skills implementing them are provided by the SARL Run-time Environment\index{SARL!SRE}\index{SARL!Run-time Environment} (SRE, see Section \ref{section:javagenerationinfra} for a description of this tool).
The SRE\index{SARL!SRE}\index{SARL!Run-time Environment} is responsible for creating them and injecting them on an agent before its execution begins.
Therefore, when the agent receives the \code{Initialize}\index{SARL!Metamodel!Event!Initialize} event, they are already available.
The current eight provided BIC, and the actions they provide along their action signatures are:
\begin{itemize}
\item \code{ExternalContextAccess}\index{SARL!Metamodel!Capacity!ExternalContextAccess} provides access to the contexts that the agent is part of, and the actions required to join and leave new contexts.
		The external context of A is shown at the top right part of Figure \ref{fig:holon}.
\item \code{InnerContextAccess}\index{SARL!Metamodel!Capacity!InnerContextAccess} provides access to the \emph{inner context} of the agent. This is the keystone for holonic agent implementation.
		The inner context of A is shown at the bottom part of Figure \ref{fig:holon}.
\item \code{Behaviors}\index{SARL!Metamodel!Capacity!Behaviors} As previously described, agent can dynamically register/unregister  behaviors and trigger them with the \code{wake} action. 
		This capacity is closely related to the \code{InnerContextAccess} to enable a high-level abstraction on holonic MAS development.
\item \code{Lifecycle}\index{SARL!Metamodel!Capacity!Lifecycle} provides actions to spawn new agents on different external contexts (peers), and the \emph{inner context} (as holonic members), as well as the \code{killMe} action to stop its own execution.
\item \code{Schedules}\index{SARL!Metamodel!Capacity!Schedules} enables the agent to schedule tasks for future or periodic execution.
\item \code{DefaultContextInteractions}\index{SARL!Metamodel!Capacity!DefaultContextInteractions} is actually provided for convenience.
		It assumes that the action is performed on the agent's \emph{default context} (upper left part of Figure \ref{fig:holon}) and its \emph{default space}.
		For instance, the \code{emit} action is a shortcut for \code{defaultContext.defaultSpace.emit(...)}.
		Therefore, it is actually created on top of the other BIC.
\item \code{Logging}\index{SARL!Metamodel!Capacity!Logging} provides the actions for writing messages on the agent's log.
		The messages may be shown according to a severity level, e.g. information, warning, error...
\item \code{Time}\index{SARL!Metamodel!Capacity!Time} provides the actions for accessing to the current time.
		The time may be the operating system time, or a simulation time, depending upon the implementation of the BIC in the SRE.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Agent's Life-cycle}

SARL does not impose a specific agent's control loop. 
Indeed, when agents are spawned, the SRE is responsible for creating the agent instance and installing the skills associated to the built-in capacities of the agent.
Then, when the agent is ready to begin its execution, the SRE fires the \code{Initialize} event. 
This event contains any parameters for the agent's instance.
Likewise, when the agent has decided to stop its own execution (using the \code{killMe} action from the \code{Lifecycle} capacity\index{SARL!Metamodel!Capacity!Lifecycle}), the SRE fires the \code{Destroy}\index{SARL!Metamodel!Event!Destroy} event to enable the agent to release any of the resources it may still hold.
It is important to notice that agents cannot kill other agents, not even those that they have spawned.
One of the key characteristics of an agent is its autonomy, and no other agent should be able to stop its execution without its consent.
The designer is then free to implement any control or authority protocol for their own application scenario.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parallel Execution}\label{section:parallelexecution}

In most of the agent frameworks, e.g. Jade \citep{Bellifemine.2007.Jade} and Janus \citep{GallandGaudRodriguezHilaire2010_20} (before its adaptation to SARL \citep{GallandRodriguezGaud2017_982}), each agent runs on a separate execution resource, i.e. a thread.
This design choice enables each agent managing its own execution resource, and participating to its autonomy.
On several other platforms, e.g. TinyMAS\footnote{TinyMAS Platform: \url{http://www.arakhne.org/tinymas}}, the agents are executed in turn in a loop. The parallel execution of the agents is therefore simulated.

Let an agent entry point be a part of the agent behavior that is reacting to a stimulus from the outside of the agent.
In SARL, the entry points are by default the behavior event handlers, specified by the \code{on} keyword.
In order to implement bio-inspired behaviors, agents may react in parallel to multiple external stimulus.
SARL encourages a massively parallel execution of agents and behaviors by associating each of these entry points to a separate thread.
Parallel execution of the pro-active behaviors of an agent is supported by the tasks that are launched with the \code{Schedules}\index{SARL!Metamodel!Capacity!ExternalContextAccess}\index{SARL!Metamodel!Capacity!Schedules} built-in capacity.

Whatever the agent execution mechanism that is implemented within the SRE (thread-based or loop-based), a SARL developer always assumes that the agent's entry points are executed in parallel when she/he is writing the event handlers.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{SARL Tool-Chain for Java Code Generation}\label{section:javagenerationinfra}

The SARL tool-chain\index{SARL!Tool-chain} is a set of programming tools that are used to perform a MAS with SARL. As illustrated by Fig. \figref{compilationprocess}, three types of tools are used in sequence in order to create and run an agent-based system:
\begin{description}
\item[SARL Compiler\index{SARL!Tool-chain!SARL Compiler}] The SARL compiler transforms the source SARL language into the target language.
		Several target languages may be considered by this compiler.
		Because most of the agent frameworks are written in Java, the SARL compiler targets this OOP language \emph{by default} (version 1.8 of the Java specifications).
		The SARL compiler translates SARL statements into their OOP equivalent statements.
		Three different implementations of the SARL compiler are provided: a specific command-line tool (\texttt{sarlc}), the Eclipse development environment plug-in, and a Maven plug-in.
\item[Java Compiler\index{SARL!Tool-chain!Java Compiler}] The files generated by the SARL compiler are standard Java's files.
		They must be compiled with one of the compilation tools that are available: Eclipse's ecl, Oracle's javac, IBM's jikes, etc.
		The result from the Java compilation is a collection of binary files (a.k.a. byte-code files) that may be run by a virtual machine.
\item[SARL Run-time Environment\index{SARL!SRE}] The SRE is a collection of tools that enable the run of an agent-based application written with SARL.
		Such a SRE must implement each service and feature that are assumed to be provided by the run-time environment.
\end{description}

\mfigure[t]{width=\linewidth}{compilation-process}{Compilation Process for the SARL programs}{compilationprocess}


In order to generate the Java code that is equivalent to the SARL code, syntactic and semantic transformations from a SARL model to an OOP model are defined by \citet{GallandRodriguez2019_1108}.
The SARL compiler is able to generate Java, Python and C\# code.

\end{graphicspathcontext}

